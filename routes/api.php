<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\API\AuthController as AuthControllerAPI;
use \App\Http\Controllers\API\MemoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route API for user management (registration, login, refreshtoken)
Route::post('register', [AuthControllerAPI::class, 'register']);
Route::post('login', [AuthControllerAPI::class, 'login']);
Route::post('refreshtoken', [AuthControllerAPI::class, 'refreshtoken']);

// Route API protected by Authentification (Password Grant Tokens)
Route::group(['middleware' => ['auth:api']], function() {

    // Route API for user management (logout)
    Route::post('logout', [AuthControllerAPI::class, 'logout']);

    // Route API for user entity
    Route::get('user', function (Request $request) {
        return $request->user();
    });

    // Route API for memo entity
    Route::apiResource('memo', MemoController::class);
});

// Route API protected by Authentification (Client Credentials Grant Tokens)
Route::group(['middleware' => ['client']], function() {
    // ...
});

