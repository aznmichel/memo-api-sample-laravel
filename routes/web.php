<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/********************ROUTES - VUEJS**********************/

/*Route::get('/{any}', function(){
    return view('layouts.app-vue');
})->where('any', '.*');*/

/********************ROUTES - BLADE**********************/

Route::get('/', function () {
    return view('welcome');
});

Route::view('home', 'home')->middleware('auth');

/************************TESTS***************************/

/*Route::get('test', function () {
    return 'Vue de test pour rediriger vers la vue de vérification d\'email';
})->middleware(['verified']);*/
