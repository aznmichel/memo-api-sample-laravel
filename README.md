<p align="center"><a href="https://memoplace-staging.herokuapp.com/" target="_blank"><img src="docs/logo-memoplace.png" width="200"></a></p>

## Demo

View a demo of the app at [Memoplace](https://memoplace-staging.herokuapp.com) (deployed in Heroku environment).

## About Memoplace

Memoplace is a simple web application built With Laravel 8 (+ VueJS) that provide an API to consume it from a SPA or a Mobile app. It Includes:

- A frontend authentication backend with [Laravel Fortify](https://github.com/laravel/fortify)
- An auth. API with [Laravel Passeport](https://laravel.com/docs/8.x/passport) which provides a full OAuth2 server implementation
- A simple frontend based on VueJS with Laravel UI ([without using Jetstream](https://www.youtube.com/watch?v=NuGBzmHlINQ&ab_channel=LaravelBusiness))
- A CI/CD pipeline with [GitLab CI](https://docs.gitlab.com/ee/ci/) that automates build, test, security and deploy steps.

This project can be used as a basis for quick prototypes or for learning. Any suggestions and recommendations are welcome!

## Getting Started

### Prerequisites

- Depends on your Laravel installation and configuration (see official [documentation](https://laravel.com/docs/8.x/installation)). 
- To run this project, you must have [PHP (>= 7.3 for Laravel 8)](https://laravel.com/docs/8.x/installation#server-requirements), [composer](https://getcomposer.org/) and [npm](https://www.npmjs.com/) installed in your machine.

_Note: In my case, I used [Laravel Homestead](https://laravel.com/docs/8.x/homestead) that provide me an entire PHP development environment (with Vagrant) to start quickly._

### Project setup
After cloning this repository to your machine:

#### 1. Install PHP dependencies:
```bash
$> composer install
```
#### 2. Update your environment settings in the .env file:
```.env
// Settings used to connect your app to a database
DB_CONNECTION=...
DB_HOST=...
DB_PORT=...
DB_DATABASE=...
DB_USERNAME=...
DB_PASSWORD=...
...
// Settings used for reset password and verify email features
MAIL_MAILER=smtp
MAIL_HOST=...
MAIL_PORT=...
MAIL_USERNAME=...
MAIL_PASSWORD=...
MAIL_ENCRYPTION=...
MAIL_FROM_ADDRESS=...
MAIL_FROM_NAME="${APP_NAME}"
...
```

#### 3. Generate a new APP_KEY:

```sh
$> php artisan key:generate
```
__*Note: For more information about the use of [APP_KEY](https://tighten.co/blog/app-key-and-you/).*__

#### 4. Run the database migrations:

```sh
php artisan migrate
```

#### 5. Configure Laravel Passeport API
Create `oauth-private.key` and `oauth-public.key`:
```sh
$> php artisan passport:keys 
```

Create Password Grant Tokens (in Database for Users):
```sh
$> php artisan passport:client --password
```
__*Note: In my case, I used Laravel Passeport with `Password Grant Tokens` configuration. For more information about [Laravel Passeport](https://laravel.com/docs/8.x/passport).*__

#### 6. Configure front-end application

Install Javascript dependencies:
```sh
$> npm install
```
Run an initial build:
```sh
$> npm run dev
```

**And that's all you need to start! Enjoy! 🚀🚀🚀**

## Information

This Memo app is build for Mobile (Flutter) and Desktop (VueJS) usage. The source code for the Mobile app in Flutter is available [here](https://gitlab.com/aznmichel/memo-mobileapp-sample-flutter) and for the Desktop app in VueJS [here](https://gitlab.com/aznmichel/memo-frontend-sample-vuejs). 

![Memoplace Presentation](docs/memoplace_presentation.png?raw=true "memoplace_presentation")

## Sources

[Laravel official documentation](https://laravel.com/docs/)
