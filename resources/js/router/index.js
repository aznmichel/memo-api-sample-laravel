import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

//import App from '../components/App'

export default new Router({
    mode: 'history',
    routes: [],
})