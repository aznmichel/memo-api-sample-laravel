<?php

namespace Database\Factories;

use App\Enums\MemoScopes;
use App\Enums\MemoStatus;
use App\Models\Memo;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MemoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Memo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => Str::random(10),
            'content' => $this->faker->paragraph,
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => User::all()->random()->user_id,
        ];
    }
}
