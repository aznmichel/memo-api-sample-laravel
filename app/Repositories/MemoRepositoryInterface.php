<?php

namespace App\Repositories;

use App\Models\Memo;
use Illuminate\Support\Collection;

interface MemoRepositoryInterface
{
    public function all(): Collection;
}
