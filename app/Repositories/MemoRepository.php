<?php

namespace App\Repositories;

use App\Models\Memo;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Collection;

class MemoRepository extends BaseRepository implements MemoRepositoryInterface
{
    /**
     * MemoRepository constructor.
     *
     * @param Memo $model
     */
    public function __construct(Memo $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }
}
