<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GeneralMemoAPIRequest;
use App\Http\Requests\StoreUpdateMemoAPIRequest;
use App\Http\Resources\MemoResource;
use App\Models\Memo;
use App\Repositories\MemoRepositoryInterface;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MemoController extends Controller
{
    private $memoRepository;

    public function __construct(MemoRepositoryInterface $memoRepository)
    {
        $this->memoRepository = $memoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    // FIXME: Passer par le memoRepository
    public function index()
    {
        $memosList = Memo::where('user_id', Auth::id())->get();
        return response([
            'memosList' => MemoResource::collection($memosList),
            'message' => 'Ressources retrouvées avec succès'
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUpdateMemoAPIRequest $request
     * @return Response
     */
    // FIXME: Passer par le memoRepository
    public function store(StoreUpdateMemoAPIRequest $request)
    {
        $memo = new Memo([
            'title' => $request->get('title'),
            'content' => $request->get('content'),
        ]);
        $memo->user()->associate(Auth::user());
        $memo->save();

        return response(['memo' => new MemoResource($memo), 'message' => 'Ressource créée avec succès'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param GeneralMemoAPIRequest $request
     * @param int $memoID
     * @return Response
     */
    public function show(GeneralMemoAPIRequest $request, int $memoID)
    {
        $memo = $this->memoRepository->find($memoID);
        return response([ 'memo' => new MemoResource($memo), 'message' => 'Ressource retrouvée avec succès'], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUpdateMemoAPIRequest $request
     * @param int $memoID
     * @return Response
     */
    // FIXME: Passer par le memoRepository
    public function update(StoreUpdateMemoAPIRequest $request, int $memoID)
    {
        $memo = $this->memoRepository->find($memoID);
        $memo->update($request->all());

        return response([ 'memo' => new MemoResource($memo), 'message' => 'Mise à jour effectuée avec succès'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param GeneralMemoAPIRequest $request
     * @param int $memoID
     * @return Response
     * @throws Exception
     */
    // FIXME: Passer par le memoRepository
    public function destroy(GeneralMemoAPIRequest $request, int $memoID)
    {
        $memo = $this->memoRepository->find($memoID);
        $memo->delete();

        return response(['message' => 'Ressource supprimée']);
    }
}
