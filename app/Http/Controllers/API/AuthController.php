<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthLoginAPIRequest;
use App\Http\Requests\AuthRegisterAPIRequest;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client as OClient;

// FIXME: Externaliser intelligence dans Repository voire Service
class AuthController extends Controller
{
    public function register(AuthRegisterAPIRequest $request)
    {
        $request['password']=Hash::make($request['password']);
        User::create($request->toArray());

        $tokens = $this->getTokenAndRefreshToken(request('email'), $request['password_confirmation']);

        return response($tokens);
    }

    public function login(AuthLoginAPIRequest $request)
    {
        if (!auth()->attempt($request->all())) {
            return response()->json(['message' => 'Invalid Credentials.'], 401);
        }
        $tokens = $this->getTokenAndRefreshToken(request('email'), request('password'));

        return response($tokens);
    }

    public function logout(Request $request)
    {
        $request->user()
            ->tokens
            ->each(function ($token) {
                $this->revokeAccessAndRefreshTokens($token->id);
            });

        return response(['message' => 'You have been successfully logged out!'], 200);
    }

    // FIXME: Externaliser dans un Service
    public function refreshToken(Request $request)
    {
        $refresh_token = $request->header('Refreshtoken');
        $oClient = OClient::where('password_client', 1)->first();

        $response = Http::asForm()->withBasicAuth($oClient->id, $oClient->secret)
            ->post(route('passport.token'), [
                'grant_type' => 'refresh_token',
                'refresh_token' => $refresh_token,
                'scope' => '*',
            ]);

        // if the status code was >= 400...
        if ($response->failed()) {
            return response()->json(['message' => 'The refresh token is invalid.'], 401);
        }
        return $response->json();
    }

    // FIXME: Externaliser dans un Service
    private function getTokenAndRefreshToken($email, $password)
    {
        $oClient = OClient::where('password_client', 1)->first();
        $response = Http::asForm()->withBasicAuth($oClient->id, $oClient->secret)
            ->post(route('passport.token'), [
                'grant_type' => 'password',
                'username' => $email,
                'password' => $password,
                'scope' => '*',
            ]);

        // Throw an exception if a client or server error occurred...
        $response->throw();

        return $response->json();
    }

    private function revokeAccessAndRefreshTokens($tokenId)
    {
        $tokenRepository = app('Laravel\Passport\TokenRepository');
        $refreshTokenRepository = app('Laravel\Passport\RefreshTokenRepository');

        $tokenRepository->revokeAccessToken($tokenId);
        $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($tokenId);
    }
}
