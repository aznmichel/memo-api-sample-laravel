<?php

namespace App\Http\Requests;

use App\Models\Memo;
use Illuminate\Validation\Rule;

class GeneralMemoAPIRequest extends AbstractAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // Récupération du paramètre memo transmis dans la route
        $memoID = $this->route('memo');
        // Enrichissement de la request avant validation des rules
        $this->merge([
            'memo' => $memoID,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // On valide le fait que le memo appartient bien à l'utilisateur courant
        return [
            'memo' => [
                Rule::exists('memos', 'id')->where(function ($query) {
                    $query->where('user_id', $this->user()->id);
                }),
            ],
        ];
    }

    public function messages()
    {
        return [
            'memo.exists' => 'La ressource demandée n\'est pas accessible.',
        ];
    }
}
