<?php

namespace App\Http\Requests;

use App\Enums\MemoScopes;
use Illuminate\Validation\Rule;

class StoreUpdateMemoAPIRequest extends AbstractAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Récupération du paramètre memo transmis dans la route
        /*$memoID = $this->route('memo');
        // Si Update, on vérifie que le memo à modifier existe bien en BD
        if(isset($memoID)) {
            return Memo::find($memoID);
            // IDEA: return $memo && $this->user()->can('update', $memo);
        }*/
        return true;
        // IDEA: return $this->user()->can('create');
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // Récupération du paramètre memo transmis dans la route
        $memoID = $this->route('memo');
        // Enrichissement de la request avant validation des rules
        $this->merge([
            'memo' => $memoID,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Rules par défaut
        $rules = [
            'title' => 'required|max:15|unique:memos,title,NULL,id,user_id,'. $this->user()->id,
            'content' => 'required|max:500',
            'scope' => Rule::in(MemoScopes::keys()),
            'status' => Rule::in(MemoScopes::keys()),
        ];

        // Récupération du paramètre memo transmis dans la route
        $memoID = $this->route('memo');
        // Si Update, enrichissement des rules de la request
        if (isset($memoID)) {
            $rules['memo'] =  [
                Rule::exists('memos', 'id')->where(function ($query) {
                    $query->where('user_id', $this->user()->id);
                }),
            ];
            $rules['title'] = 'required|max:15|unique:memos,title,'. $memoID .',id,user_id,'. $this->user()->id;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'memo.exists' => 'La ressource demandée n\'est pas accessible.',
        ];
    }
}
