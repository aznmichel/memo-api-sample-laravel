<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The MemoScopes enum.
 *
 * @method static self PUBLIC()
 * @method static self PRIVATE()
 */
class MemoScopes extends Enum
{
    const PUBLIC = 0;
    const PRIVATE = 1;

    /**
     * Retrieve a map of enum keys and values.
     *
     * @return array
     */
    public static function map() : array
    {
        return [
            static::PUBLIC => 'Public',
            static::PRIVATE => 'Private',
        ];
    }
}
