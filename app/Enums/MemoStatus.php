<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The MemoStatus enum.
 *
 * @method static self OPEN()
 * @method static self CLOSED()
 */
class MemoStatus extends Enum
{
    const OPEN = 0;
    const CLOSED = 1;

    /**
     * Retrieve a map of enum keys and values.
     *
     * @return array
     */
    public static function map() : array
    {
        return [
            static::OPEN => 'Open',
            static::CLOSED => 'Closed',
        ];
    }
}
