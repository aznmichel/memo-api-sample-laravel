<?php

namespace API;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AuthAPIControllerTest extends TestCase
{
    // Run migration BD
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('passport:install');
        Artisan::call('passport:client --client --name "Laravel ClientCredentials Grant Client"');
    }

    public function testShouldRegisterNewUser()
    {
        Http::fake([
            route('passport.token') => Http::response([
                'token_type' => 'Bearer',
                'expires_in' => '86400',
                'access_token' => 'eyJ0eXAiOiJKV1QiLC...JhbGciOiJSUzI1NiJ9',
                'refresh_token' => 'def50200024c8892047...f0a74596d08af3dee2d18ddfed4503ddcb',
            ], 200),
        ]);

        $response = $this->post('/api/register', [
            'name' => 'John Doe',
            'email' => 'john@me.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'name' => 'John Doe',
            'email' => 'john@me.com',
        ]);
    }

    public function testShouldNotRegisterExistingUser()
    {
        User::factory()->create([
            'name' => 'John Doe',
            'email' => 'john@me.com',
            'password' => 'password',
        ]);

        $response = $this->post('/api/register', [
            'name' => 'John Doe',
            'email' => 'john@me.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $response->assertStatus(422);
        $response->assertJson(["error" => ["email" => ["The email has already been taken."]],"message" => "Erreur(s) de validation"]);
    }

    public function testShouldLoginUserWithValidCredentials()
    {
        User::factory()->create([
            'name' => 'John Doe',
            'email' => 'john@me.com',
            'password' => bcrypt($password = 'i-love-laravel'),
        ]);

        Http::fake([
            route('passport.token') => Http::response([
                'token_type' => 'Bearer',
                'expires_in' => '86400',
                'access_token' => 'eyJ0eXAiOiJKV1QiLC...JhbGciOiJSUzI1NiJ9',
                'refresh_token' => 'def50200024c8892047...f0a74596d08af3dee2d18ddfed4503ddcb',
            ], 200),
        ]);

        $response = $this->post('/api/login', [
            'email' => 'john@me.com',
            'password' => $password,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['access_token']);
        $response->assertJsonStructure(['refresh_token']);
    }

    public function testShouldLoginUserWithInvalidCredentials()
    {
        User::factory()->create([
            'name' => 'John Doe',
            'email' => 'john@me.com',
            'password' => bcrypt($password = 'i-love-laravel'),
        ]);

        $response = $this->post('/api/login', [
            'email' => 'john@me.com',
            'password' => $password . 'wrong',
        ]);

        $response->assertStatus(401);
        $response->assertJson(['message' => 'Invalid Credentials.']);
    }

    public function testUserShouldLogoutCurrentUser()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        Passport::actingAsClient(
            Client::factory()->create(),
        );

        $response = $this->json('POST', '/api/logout');

        $response->assertStatus(200);
        $response->assertJson(['message' => 'You have been successfully logged out!']);
    }

    public function testShouldRevokeAccessAndRefreshTokensWhenLogout()
    {
        $user = User::factory()->create([
            'name' => 'John Doe',
            'email' => 'john@me.com',
            'password' => bcrypt($password = 'i-love-laravel'),
        ]);
        Passport::actingAs($user);

        Passport::actingAsClient(
            Client::factory()->create(),
        );

        $this->json('POST', '/api/logout')->assertStatus(200);

        // Refresh de l'instance de l'application
        $this->refreshApplication();

        $response = $this->json('GET', 'api/user');
        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    public function testShouldGetNewTokensWithValidRefreshToken()
    {
        $inputs = [
            'current_refresh_token' => 'def50200024c8892047...f0a74596d08af3dee2d18ddfed4503ddcbREFRESHTOKEN',
            'new_access_token' => 'yJ0eXAiOiJKV1QiLC...JhbGciOiJSUzI1NiJ9NEWACCESSTOKEN',
            'new_refresh_token' => 'def50200024c8892047...f0a74596d08af3dee2d18ddfed4503ddcbNEWREFRESHTOKEN'
        ];

        Http::fake(function ($request) use ($inputs) {
            $refresh_token = $request->data()['refresh_token'];
            if($refresh_token == $inputs['current_refresh_token']) {
                return Http::response([
                    'token_type' => 'Bearer',
                    'expires_in' => '86400',
                    'access_token' => $inputs['new_access_token'],
                    'refresh_token' => $inputs['new_refresh_token'],
                ], 200);
            }
            else {
                return Http::response([
                    "message" => "The refresh token is invalid."
                ], 401);
            }
        });

        $responseRefreshToken = $this->post('/api/refreshtoken', [], [
            'Refreshtoken' => $inputs['current_refresh_token']  // Je fournis un Refreshtoken VALIDE
        ]);

        $responseRefreshToken->assertStatus(200);
        $responseRefreshToken->assertJsonStructure(['access_token']);
        $responseRefreshToken->assertJsonStructure(['refresh_token']);
        $this->assertEquals($responseRefreshToken['access_token'], $inputs['new_access_token']);
        $this->assertEquals($responseRefreshToken['refresh_token'], $inputs['new_refresh_token']);
    }

    public function testShouldGetNewTokensWithInvalidRefreshToken()
    {
        $inputs = [
            'current_refresh_token' => 'def50200024c8892047...f0a74596d08af3dee2d18ddfed4503ddcbREFRESHTOKEN',
            'new_access_token' => 'yJ0eXAiOiJKV1QiLC...JhbGciOiJSUzI1NiJ9NEWACCESSTOKEN',
            'new_refresh_token' => 'def50200024c8892047...f0a74596d08af3dee2d18ddfed4503ddcbNEWREFRESHTOKEN'
        ];

        Http::fake(function ($request) use ($inputs) {
            $refresh_token = $request->data()['refresh_token'];
            if($refresh_token == $inputs['current_refresh_token']) {
                return Http::response([
                    'token_type' => 'Bearer',
                    'expires_in' => '86400',
                    'access_token' => $inputs['new_access_token'],
                    'refresh_token' => $inputs['new_refresh_token'],
                ], 200);
            }
            else {
                return Http::response([
                    "message" => "The refresh token is invalid."
                ], 401);
            }
        });

        $responseRefreshToken = $this->post('/api/refreshtoken', [], [
            'Refreshtoken' => 'INVALIDREFRESHTOKEN' // Je fournis un Refreshtoken INVALIDE
        ]);

        $responseRefreshToken->assertStatus(401);
        $responseRefreshToken->assertJson(['message' => 'The refresh token is invalid.']);
    }

    public function testShouldNotAccessUserDetailsIfTokenRevokedAfterRefreshToken()
    {
        $user = User::factory()->create([
            'name' => 'John Doe',
            'email' => 'john@me.com',
            'password' => bcrypt($password = 'i-love-laravel'),
        ]);
        Passport::actingAs($user);

        Passport::actingAsClient(
            Client::factory()->create(),
        );

        $inputs = [
            'current_refresh_token' => 'def50200024c8892047...f0a74596d08af3dee2d18ddfed4503ddcbREFRESHTOKEN',
            'new_access_token' => 'yJ0eXAiOiJKV1QiLC...JhbGciOiJSUzI1NiJ9NEWACCESSTOKEN',
            'new_refresh_token' => 'def50200024c8892047...f0a74596d08af3dee2d18ddfed4503ddcbNEWREFRESHTOKEN'
        ];

        Http::fake(function ($request) use ($inputs) {
            $refresh_token = $request->data()['refresh_token'];
            if($refresh_token == $inputs['current_refresh_token']) {
                return Http::response([
                    'token_type' => 'Bearer',
                    'expires_in' => '86400',
                    'access_token' => $inputs['new_access_token'],
                    'refresh_token' => $inputs['new_refresh_token'],
                ], 200);
            }
            else {
                return Http::response([
                    "message" => "The refresh token is invalid."
                ], 401);
            }
        });

        $this->post('/api/refreshtoken', [], [
            'Refreshtoken' => $inputs['current_refresh_token']  // Je fournis un Refreshtoken VALIDE
        ]);

        // Refresh de l'instance de l'application
        $this->refreshApplication();

        $response = $this->json('GET', 'api/user');
        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }
}
