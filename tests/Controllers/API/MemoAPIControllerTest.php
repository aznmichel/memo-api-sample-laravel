<?php

namespace API;

use App\Enums\MemoScopes;
use App\Enums\MemoStatus;
use App\Models\Memo;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;
use Tests\TestCase;

class MemoAPIControllerTest extends TestCase
{
    // Run migration BD
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('passport:install');
        Artisan::call('passport:client --client --name "Laravel ClientCredentials Grant Client"');
    }

    private function initUserAndClient() : User {
        $user = User::factory()->create();
        Passport::actingAs($user);

        Passport::actingAsClient(
            Client::factory()->create(),
        );

        return $user;
    }

    public function testUserShouldNotAccessMemoWithoutLogin()
    {
        $response = $this->get('/api/memo');
        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    public function testUserShouldNotCreateMemoWithoutLogin()
    {
        $response = $this->post('/api/memo');

        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    public function testUserShouldAccessMemoWithLogin()
    {
        $this->initUserAndClient();

        $response = $this->get('/api/memo');
        $response->assertStatus(200);
    }

    public function testUserShouldCreateMemoWithLogin()
    {
        $user = $this->initUserAndClient();

        $response = $this->json('POST', '/api/memo', [
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas("memos", [
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);
    }

    public function testUserShouldNotCreateMemoWithWrongScopeAndStatus()
    {
        $user = $this->initUserAndClient();

        $response = $this->json('POST', '/api/memo', [
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => 99,
            'status' => 99,
            'user_id' => $user->id
        ]);

        $response->assertStatus(422);
    }

    public function testUserShouldUpdateExistingMemoOfHim()
    {
        $user = $this->initUserAndClient();

        $memo = Memo::factory()->create([
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $response = $this->json('PUT', '/api/memo/' . $memo->id, [
            'title' => 'MEMO TEST MAJ',
            'content' => 'BLABLABLA MAJ',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO TEST MAJ',
            'content' => 'BLABLABLA MAJ',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);
    }

    public function testUserShouldNotUpdateExistingMemoOfAnOtherUser()
    {
        $user1 = User::factory()->create(['name' => 'john doe 1']);
        $user2 = User::factory()->create(['name' => 'john doe 2']);

        Passport::actingAs($user1);

        Passport::actingAsClient(
            Client::factory()->create(),
        );

        $memo = Memo::factory()->create([
            'title' => 'MEMO john doe 2',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user2->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO john doe 2',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user2->id
        ]);

        $response = $this->json('PUT', '/api/memo/' . $memo->id, [
            'title' => 'MEMO john doe 1 MAJ',
            'content' => 'BLABLABLA MAJ',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
        ]);

        $response->assertStatus(422);
        $response->assertJson(["error" => ["memo" => ["La ressource demandée n'est pas accessible."]],"message" => "Erreur(s) de validation"]);
    }

    public function testUserShouldNotUpdateNonExistingMemo()
    {
        $user = $this->initUserAndClient();

        $memo = Memo::factory()->create([
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $wrong_id = $memo->id+1;
        $response = $this->json('PUT', '/api/memo/' . $wrong_id, [
            'title' => 'MEMO TEST MAJ',
            'content' => 'BLABLABLA MAJ',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $response->assertStatus(422);
        $response->assertJson(["error" => ["memo" => ["La ressource demandée n'est pas accessible."]],"message" => "Erreur(s) de validation"]);
    }

    public function testUserShouldDeleteExistingMemoOfHim()
    {
        $user = $this->initUserAndClient();

        $memo = Memo::factory()->create([
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $response = $this->json('DELETE', '/api/memo/' . $memo->id);

        $response->assertStatus(200);
        $this->assertDatabaseMissing("memos", [
            'id' => $memo->id,
            'title' => 'MEMO TEST MAJ',
            'content' => 'BLABLABLA MAJ',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);
    }

    public function testUserShouldNotDeleteExistingMemoOfAnOtherUser()
    {
        $user1 = User::factory()->create(['name' => 'john doe 1']);
        $user2 = User::factory()->create(['name' => 'john doe 2']);

        Passport::actingAs($user1);

        Passport::actingAsClient(
            Client::factory()->create(),
        );

        $memo = Memo::factory()->create([
            'title' => 'MEMO john doe 2',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user2->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO john doe 2',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user2->id
        ]);

        $response = $this->json('DELETE', '/api/memo/' . $memo->id);

        $response->assertStatus(422);
        $response->assertJson(["error" => ["memo" => ["La ressource demandée n'est pas accessible."]],"message" => "Erreur(s) de validation"]);
    }

    public function testUserShouldNotDeleteNonExistingMemo()
    {
        $user = $this->initUserAndClient();

        $memo = Memo::factory()->create([
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $wrong_id = $memo->id+1;
        $response = $this->json('DELETE', '/api/memo/' . $wrong_id);

        $response->assertStatus(422);
        $response->assertJson(["error" => ["memo" => ["La ressource demandée n'est pas accessible."]],"message" => "Erreur(s) de validation"]);
    }

    public function testUserShouldDisplayExistingMemoOfHim()
    {
        $user = $this->initUserAndClient();

        $memo = Memo::factory()->create([
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $response = $this->json('GET', '/api/memo/' . $memo->id);

        $response->assertStatus(200);
    }

    public function testUserShouldNotDisplayExistingMemoOfAnOtherUser()
    {
        $user1 = User::factory()->create(['name' => 'john doe 1']);
        $user2 = User::factory()->create(['name' => 'john doe 2']);

        Passport::actingAs($user1);

        Passport::actingAsClient(
            Client::factory()->create(),
        );

        $memo = Memo::factory()->create([
            'title' => 'MEMO john doe 2',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user2->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO john doe 2',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user2->id
        ]);

        $response = $this->json('GET', '/api/memo/' . $memo->id);

        $response->assertStatus(422);
        $response->assertJson(["error" => ["memo" => ["La ressource demandée n'est pas accessible."]],"message" => "Erreur(s) de validation"]);
    }

    public function testUserShouldNotDisplayNonExistingMemo()
    {
        $user = $this->initUserAndClient();

        $memo = Memo::factory()->create([
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas("memos", [
            'id' => $memo->id,
            'title' => 'MEMO TEST',
            'content' => 'BLABLABLA',
            'scope' => MemoScopes::PUBLIC,
            'status' => MemoStatus::OPEN,
            'user_id' => $user->id
        ]);

        $wrong_id = $memo->id+1;
        $response = $this->json('GET', '/api/memo/' . $wrong_id);

        $response->assertStatus(422);
        $response->assertJson(["error" => ["memo" => ["La ressource demandée n'est pas accessible."]],"message" => "Erreur(s) de validation"]);
    }
}
